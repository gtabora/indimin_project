﻿using System;
using System.Collections.Generic;
using Backend.Domain.Common;

namespace Backend.Domain.Entities
{
    public class Day : AuditableEntity, IHasDomainEvent
    {
        public Day()
        {
            DomainEvents = new List<DomainEvent>();
        }
        public int Id { get; set; }
        public string DayName { get; set; }
        public List<DomainEvent> DomainEvents { get; set; }

        public List<AssignmentByCitizen> AssignmentByCitizens { get; set; }
    }
}
