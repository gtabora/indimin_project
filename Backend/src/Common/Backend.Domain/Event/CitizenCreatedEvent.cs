using Backend.Domain.Common;
using Backend.Domain.Entities;

namespace Backend.Domain.Event
{
    public class CitizenCreatedEvent : DomainEvent
    {
        public CitizenCreatedEvent(Citizen citizen)
        {
            Citizen = citizen;
        }

        public Citizen Citizen { get; }
    }
}
