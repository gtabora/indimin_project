﻿using System.Linq;
using System.Threading.Tasks;
using Backend.Infrastructure.Identity;
using Backend.Domain.Entities;
using Microsoft.AspNetCore.Identity;

namespace Backend.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            var administratorRole = new IdentityRole("Administrator");

            if (roleManager.Roles.All(r => r.Name != administratorRole.Name))
            {
                await roleManager.CreateAsync(administratorRole);
            }

            var defaultUser = new ApplicationUser { UserName = "gtabora", Email = "test@test.com" };

            if (userManager.Users.All(u => u.UserName != defaultUser.UserName))
            {
                await userManager.CreateAsync(defaultUser, "gtabora");
                await userManager.AddToRolesAsync(defaultUser, new[] { administratorRole.Name });
            }
        }

        public static async Task SeedSampleCitizensDataAsync(ApplicationDbContext context)
        {
            if(!context.Citizens.Any()){
                context.Citizens.Add(new Citizen{
                    FirstName="Gaby",
                    LastName="Tabora",
                    Active=true
                });
                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedSampleAssignmentsDataAsync(ApplicationDbContext context)
        {
            if (!context.Assignments.Any())
            {
                context.Assignments.Add(new Assignment
                {
                    Title="Clean",
                    Description="Clean the whitch's office",
                    Active=true
                });
                context.Assignments.Add(new Assignment
                {
                    Title = "Collect gemstones",
                    Description = "Collect gemstones from the mine",
                    Active = true
                });
                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedSampleDaysDataAsync(ApplicationDbContext context)
        {
            if (!context.Days.Any())
            {
                context.Days.Add(new Day
                {
                    DayName="Sunday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Monday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Tuesday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Wednesday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Thursday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Friday"
                });
                context.Days.Add(new Day
                {
                    DayName = "Saturday"
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
