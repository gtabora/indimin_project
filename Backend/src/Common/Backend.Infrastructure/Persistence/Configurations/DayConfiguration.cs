﻿using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ElOjo.Backend.Infrastructure.Persistence.Configurations
{
    public class DayConfiguration : IEntityTypeConfiguration<Day>
    {
        public void Configure(EntityTypeBuilder<Day> builder)
        {
            builder.Ignore(e => e.DomainEvents);

            builder.Property(t => t.DayName)
                .HasMaxLength(10)
                .IsRequired();
          
        }
    }
}
