using Backend.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Backend.Infrastructure.Persistence.Configurations
{
    public class CitizenConfiguration : IEntityTypeConfiguration<Citizen>
    {
        public void Configure(EntityTypeBuilder<Citizen> builder)
        {
            builder.Ignore(e => e.DomainEvents);

            builder.Property(t => t.FirstName)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(t => t.LastName)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(t => t.Active)
                .IsRequired();
        }
    }
}
