﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Days.Queries.GetCurrentDay
{
    public class GetCurrentDayQuery : IRequestWrapper<DayDto>
    {
        public string CurrentDate { get; set; }
    }

    public class GetCurrentDayQueryHandler : IRequestHandlerWrapper<GetCurrentDayQuery, DayDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCurrentDayQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<DayDto>> Handle(GetCurrentDayQuery request, CancellationToken cancellationToken)
        {
            var currentDay = (int)Convert.ToDateTime(request.CurrentDate).DayOfWeek;

            var day = await _context.Days.FirstOrDefaultAsync(x => x.Id == currentDay).ConfigureAwait(false);

            if (day == null) return ServiceResult.Success(new DayDto { Id = 1, DayName = "Sunday" });

            var currentDayDto = new DayDto
            {
                Id=day.Id,
                DayName=day.DayName
            };

            return currentDayDto != null ? ServiceResult.Success(currentDayDto) : ServiceResult.Failed<DayDto>(ServiceError.NotFount);
        }
    }
}
