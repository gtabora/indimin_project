﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Days.Queries.GetAllDays
{
    public class GetAllDaysQuery : IRequestWrapper<List<DayDto>>
    {

    }

    public class GetAllDaysQueryHandler : IRequestHandlerWrapper<GetAllDaysQuery, List<DayDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllDaysQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<DayDto>>> Handle(GetAllDaysQuery request, CancellationToken cancellationToken)
        {
            List<DayDto> list = await _context.Days
                .ProjectToType<DayDto>(_mapper.Config)
                .ToListAsync(cancellationToken);

            return list.Count > 0 ? ServiceResult.Success(list) : ServiceResult.Failed<List<DayDto>>(ServiceError.NotFount);
        }
    }
}
