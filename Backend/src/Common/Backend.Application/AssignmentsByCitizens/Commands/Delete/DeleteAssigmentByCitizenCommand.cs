using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Exceptions;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.AssignmentsByCitizens.Commands.Delete
{
    public class DeleteAssignmentsByCitizensCommand : IRequestWrapper<AssignmentByCitizenDto>
    {
        public int Id { get; set; }
    }

    public class DeleteAssignmentsByCitizenCommandHandler : IRequestHandlerWrapper<DeleteAssignmentsByCitizensCommand, AssignmentByCitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DeleteAssignmentsByCitizenCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentByCitizenDto>> Handle(DeleteAssignmentsByCitizensCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.AssignmentByCitizens
                .Where(l => l.Id == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Assignment), request.Id);
            }

            _context.AssignmentByCitizens.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentByCitizenDto>(entity));
        }
    }
}
