using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.AssignmentsByCitizens.Queries.GetAssignmentsPerDay
{
    public class GetAssignmentsPerDayQuery : IRequestWrapper<List<AssignmentByCitizenDto>>
    {
        public int Day { get; set; }
    }

    public class GetAssignmentsPerDayQueryHandler : IRequestHandlerWrapper<GetAssignmentsPerDayQuery, List<AssignmentByCitizenDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAssignmentsPerDayQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<AssignmentByCitizenDto>>> Handle(GetAssignmentsPerDayQuery request, CancellationToken cancellationToken)
        {
            var assignmentByCitizen = await _context.AssignmentByCitizens
                .Where(x => x.DayId == request.Day)
                .Include(x=>x.Citizen)
                .ProjectToType<AssignmentByCitizenDto>(_mapper.Config).ToListAsync(cancellationToken);

            if (assignmentByCitizen == null) return ServiceResult.Success(new List<AssignmentByCitizenDto>());

            assignmentByCitizen.ForEach(y =>
            {
                var citizen = _context.Citizens.FirstOrDefault(x => x.Id == y.CitizenId);

                if (citizen != null)
                {
                    y.FullNameCitizen = citizen.GetFullName();
                }

                var assignment = _context.Assignments.FirstOrDefault(x => x.Id == y.AssignmentId);

                if (citizen != null)
                {
                    y.TitleAssignment = assignment.Title;
                }

                var day = _context.Days.FirstOrDefault(x => x.Id == y.DayId);

                if (day != null)
                {
                    y.DayName = day.DayName;
                }
            });

           

            return assignmentByCitizen != null ? ServiceResult.Success(assignmentByCitizen) : ServiceResult.Failed<List<AssignmentByCitizenDto>>(ServiceError.NotFount);
        }
    }
}
