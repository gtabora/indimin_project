﻿using System.Threading.Tasks;
using Backend.Application.Common.Models;

namespace Backend.Application.Common.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest request);
    }
}
