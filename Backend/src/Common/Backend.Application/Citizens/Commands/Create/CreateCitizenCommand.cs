using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using Backend.Domain.Event;
using MapsterMapper;

namespace ElOjo.Backend.Application.Citizens.Commands.Create
{
    public class CreateCitizenCommand : IRequestWrapper<CitizenDto>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

    public class CreateCityCommandHandler : IRequestHandlerWrapper<CreateCitizenCommand, CitizenDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CreateCityCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<CitizenDto>> Handle(CreateCitizenCommand request, CancellationToken cancellationToken)
        {
            var entity = new Citizen
            {
                FirstName = request.FirstName,
                LastName=request.LastName,
                Active=true
            };

            entity.DomainEvents.Add(new CitizenCreatedEvent(entity));

            await _context.Citizens.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<CitizenDto>(entity));
        }
    }
}
