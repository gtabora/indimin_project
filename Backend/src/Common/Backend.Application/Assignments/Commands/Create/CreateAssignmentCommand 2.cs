using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Backend.Domain.Entities;
using Backend.Domain.Event;
using MapsterMapper;

namespace Backend.Application.Assignments.Commands.Create
{
    public class CreateAssignmentCommand : IRequestWrapper<AssignmentDto>
    {
        public string Title { get; set; }
        public string Description { get; set; }

    }

    public class CreateAssignmentCommandHandler : IRequestHandlerWrapper<CreateAssignmentCommand, AssignmentDto>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public CreateAssignmentCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<AssignmentDto>> Handle(CreateAssignmentCommand request, CancellationToken cancellationToken)
        {
            var entity = new Assignment
            {
                Title = request.Title,
                Description=request.Description,
                Active=true
            };

            entity.DomainEvents.Add(new AssignmentCreatedEvent(entity));

            await _context.Assignments.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return ServiceResult.Success(_mapper.Map<AssignmentDto>(entity));
        }
    }
}
