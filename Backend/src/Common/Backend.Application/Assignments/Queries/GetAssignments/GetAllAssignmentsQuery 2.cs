using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Interfaces;
using Backend.Application.Common.Models;
using Backend.Application.Dto;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace Backend.Application.Assignments.Queries.GetAssignments
{
    public class GetAllAssignmentsQuery : IRequestWrapper<List<AssignmentDto>>
    {

    }

    public class GetAllAssignmentsQueryHandler : IRequestHandlerWrapper<GetAllAssignmentsQuery, List<AssignmentDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllAssignmentsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<List<AssignmentDto>>> Handle(GetAllAssignmentsQuery request, CancellationToken cancellationToken)
        {
            List<AssignmentDto> list = await _context.Assignments
                .ProjectToType<AssignmentDto>(_mapper.Config)
                .ToListAsync(cancellationToken);

            return list.Count > 0 ? ServiceResult.Success(list) : ServiceResult.Failed<List<AssignmentDto>>(ServiceError.NotFount);
        }
    }
}
