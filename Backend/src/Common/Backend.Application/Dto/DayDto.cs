﻿using System;
namespace Backend.Application.Dto
{
    public class DayDto
    {
        public int Id { get; set; }
        public string DayName { get; set; }
        public bool Selected { get; set; }
    }
}
