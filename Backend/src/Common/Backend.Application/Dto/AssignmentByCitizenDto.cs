using System.Collections.Generic;
using Backend.Domain.Entities;
using Mapster;

namespace Backend.Application.Dto
{
    public class AssignmentByCitizenDto : IRegister
    {
        public AssignmentByCitizenDto()
        {
        }

        public int Id { get; set; }
        public int CitizenId { get; set; }
        public int AssignmentId { get; set; }
        public string Status { get; set; }
        public int DayId{get;set;}
        public string CreateDate { get; set; }
        public string FullNameCitizen { get; set; }
        public string TitleAssignment { get; set; }
        public string DayName { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<AssignmentByCitizen, AssignmentByCitizenDto>()
            .Map(dest => dest.CreateDate,
                src => $"{src.CreateDate.ToShortDateString()}");

            config.NewConfig<Citizen, CitizenDto>()
               .Map(dest => dest.FirstName, src => src.FirstName)
               .Map(dest => dest.LastName, src => src.LastName);

            config.NewConfig<Assignment, AssignmentDto>()
              .Map(dest => dest.Title, src => src.Title)
              .Map(dest => dest.Description, src => src.Description);
        }
    }
}
