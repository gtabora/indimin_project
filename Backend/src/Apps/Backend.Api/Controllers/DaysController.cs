﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Backend.Application.Common.Models;
using Backend.Application.Days.Queries.GetAllDays;
using Backend.Application.Days.Queries.GetCurrentDay;
using Backend.Application.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{

    public class DaysController : BaseApiController
    {
        [HttpGet]
        public async Task<ActionResult<ServiceResult<List<DayDto>>>> GetAllDays(CancellationToken cancellationToken)
        {
            //Cancellation token example.
            return Ok(await Mediator.Send(new GetAllDaysQuery(), cancellationToken));
        }

        [HttpGet("{currentDate}")]
        public async Task<ActionResult<ServiceResult<DayDto>>> GetCurrentDay(string currentDate)
        {
            //Cancellation token example.
            return Ok(await Mediator.Send(new GetCurrentDayQuery() { CurrentDate = currentDate }));
        }
    }
}
