﻿using System.Threading.Tasks;
using Backend.Application.ApplicationUser.Queries.GetToken;
using Backend.Application.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Api.Controllers
{
    public class LoginController : BaseApiController
    {
        [HttpPost]
        public async Task<ActionResult<ServiceResult<LoginResponse>>> Create(GetTokenQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
